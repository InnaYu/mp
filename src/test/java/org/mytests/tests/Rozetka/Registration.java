package org.mytests.tests.Rozetka;

import api.tempMail.TempMailApi;
import io.qameta.allure.Step;
import org.apache.commons.lang3.StringUtils;
import org.mytests.tests.TestsInit;
import org.mytests.uiobjects.example.BaseDataProvider;
import org.mytests.uiobjects.example.entities.User;
import org.mytests.uiobjects.example.site.sections.ConfirmForm;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mytests.uiobjects.example.site.RozetkaSite.homePage;
import static org.mytests.uiobjects.example.site.RozetkaSite.personalInformation;
import static org.mytests.uiobjects.example.site.sections.LoginForm.registration;
import static org.mytests.uiobjects.example.site.sections.LoginForm.registrationForm;

public class Registration implements TestsInit {

    @Test
    public void PositiveRegistration() throws IOException {
        User user = BaseDataProvider.generateNewUser();
        registrationNewUser(user);
        String regCode = getRegistrationCode(user.getEmail());
        confirmEmail(regCode, user);
    }


    private String getRegistrationCode(String email) throws IOException {
        String response = TempMailApi.get(email).body().string();
        String registrationCode = StringUtils.substringBetween(response, "подтверждения регистрации: ", "\\n");
        return registrationCode;
    }

    @Step
    private void registrationNewUser(User user){
        homePage.loginFormButton.click();
        registration.click();
        registrationForm.submit(user);

    }
    @Step
    private void confirmEmail(String regCode, User user){
        personalInformation.open();
        personalInformation.contacts.editButton.click();
        personalInformation.contacts.confirmButton.click();
        ConfirmForm confirmForm = personalInformation.contacts.confirmForm;
        confirmForm.code.sendKeys(regCode);
        confirmForm.submit.click();

        Assert.assertEquals(confirmForm.confirmEmailMessage.getText(), " Эл. почта " + user.getEmail() + " подтверждена ",
                "Email is not confirmed");

    }

}
