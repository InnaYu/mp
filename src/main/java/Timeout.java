import com.epam.jdi.light.settings.Timeouts;

public enum Timeout {

    ZERO(0),
    ONE(1),
    MICRO(2),
    THREE_SECOND(3),
    SMALL(5),
    TEN_SECOND(10),
    MEDIUM(15),
    LONG(30),
    EXTRALONG(45),
    ONE_MINUTE(60),
    TWO_MINUTES(120),
    FOUR_MINUTES(240),
    FIVE_MINUTES(300),
    SIX_MINUTES(360),
    EIGHT_MINUTES(480),
    TEN_MINUTES(600),
    FIFTEEN_MINUTES(900),
    SIXTEEN_MINUTES(960),
    TWENTY_MINUTES(1200),
    THIRTY_MINUTES(1800),
    FORTY_MINUTES(2400),
    ONE_HOUR(3600),
    ONE_HOUR_THIRTY_MINUTES(5400),
    ;

    public int timeout;

    Timeout(int timeout) {
        this.timeout = timeout;
    }

    public long getTimeoutMilliseconds() {
        return timeout * 1000;
    }

    @Override
    public String toString() {
        return timeout + " seconds";
    }
}

