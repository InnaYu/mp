package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.oracle.javafx.jmx.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

public class JsonUtil {
    public static ObjectMapper objectMapper = new ObjectMapper();

    static {
        setUpObjectMapper(objectMapper);
    }


    private static void setUpObjectMapper(ObjectMapper objectMapper) {
        objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
                .setPropertyNamingStrategy(new CobraCaseX())
                .disable(WRITE_DATES_AS_TIMESTAMPS)
                .setDateFormat(new StdDateFormat().withColonInTimeZone(true))
                .enable(MapperFeature.USE_STD_BEAN_NAMING);
    }

    public static class CobraCaseX extends PropertyNamingStrategy.PropertyNamingStrategyBase {
        private static final Pattern REGEX = Pattern.compile("[A-Z]");

        @Override
        public String translate(String input) {
            if (input == null)
                return input; // garbage in, garbage out

            if (!input.isEmpty() && Character.isUpperCase(input.charAt(0)))
                input = input.substring(0, 1).toLowerCase() + input.substring(1);

            return REGEX.matcher(input).replaceAll("$0");
        }
    }

}
