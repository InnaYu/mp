package util;

public class Random {


        public static int genInt(int from, int to) {
            int tmp = 0;
            if (to >= from)
                tmp = (int) (from + Math.round(Math.random() * (to - from)));
            return tmp;
        }

        public static String genRandomAlphabeticString(int length, int from, int to) {
            if (length < 0) {
                throw new IllegalArgumentException("How do you except me to generate a string of negative length?");
            }
            if (length == 0) {
                return "";
            }
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; ++i) {
                sb.append((char) genInt( from, to));
            }
            return sb.toString();
        }

    public static String genPassword() {
        return genRandomAlphabeticString(2, 'a','z').toLowerCase() + genRandomAlphabeticString(2, 'a','z').toUpperCase()
                + "#" + genInt(111, 999);
    }
}
