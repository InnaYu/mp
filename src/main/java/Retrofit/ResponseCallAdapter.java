package Retrofit;

import io.qameta.allure.Step;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;

import java.lang.reflect.Type;

import static Retrofit.CallAdapterHelper.getResponse;

public class ResponseCallAdapter <T> implements CallAdapter<T, Response<T>> {
    private Type returnType;

    ResponseCallAdapter(Type returnType) {
        this.returnType = returnType;
    }

    @Override
    public Type responseType() {
        return returnType;
    }

    @Override
    public Response<T> adapt(Call<T> call) {
        return requestAttributesToLog(call.request().method(), call.request().url(), call);
    }

    @Step("{method} {url}")
    private Response<T> requestAttributesToLog(String method, HttpUrl url, Call<T> call) {
        return getResponse(call);
    }
}


