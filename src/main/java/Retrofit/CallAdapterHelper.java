package Retrofit;

import com.epam.jdi.light.common.Timeout;
import org.apache.hc.core5.http.HttpStatus;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class CallAdapterHelper {
    static Response getResponse(Call call) {
        Response response = executeCall(call);
        return response;

    }

    private static Response executeCall(Call call) {
        try {
            return call.execute();
        } catch (IOException e) {
            throw new RuntimeException("Could not execute request", e);
        }
    }

    }