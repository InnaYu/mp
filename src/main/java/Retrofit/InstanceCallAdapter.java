package Retrofit;

import io.qameta.allure.Step;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.CallAdapter;

import java.lang.reflect.Type;

import static Retrofit.CallAdapterHelper.getResponse;

public class InstanceCallAdapter <T> implements CallAdapter<T, Object> {
    private Type returnType;

    InstanceCallAdapter(Type returnType) {
        this.returnType = returnType;
    }

    @Override
    public Type responseType() {
        return returnType;
    }

    @Override
    public Object adapt(Call<T> call) {
        return getRequest(call).body();
    }

    private retrofit2.Response<T> getRequest(Call<T> call) {
        return requestAttributesToLog(call.request().method(), call.request().url(), call);
    }

    @Step("{method} {url}")
    private retrofit2.Response<T> requestAttributesToLog(String method, HttpUrl url, Call<T> call) {
        return getResponse(call);
    }

}
