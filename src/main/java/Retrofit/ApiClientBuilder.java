package Retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import util.JsonUtil;

import java.io.IOException;

public class ApiClientBuilder {

    public static final String BASE_API_URL = "https://privatix-temp-mail-v1.p.rapidapi.com/request/";
    public static final String HOST = "privatix-temp-mail-v1.p.rapidapi.com";
    public static final String KEY = "5bbded4c66msh4876af356a956e2p1ef0efjsn5606463472a0";


    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
    }

    public static <T> T build(Class<T> tClass) {
        Retrofit retrofit;
        OkHttpClient client = makeHttpClient();


        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addCallAdapterFactory(new ApiCallAdapter<T>())
                .addConverterFactory(JacksonConverterFactory.create(JsonUtil.objectMapper))
                .client(client)
                .build();
        return retrofit.create(tClass);

    }



    public static OkHttpClient makeHttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request originalRequest = chain.request();

                Request.Builder builder = originalRequest.newBuilder().addHeader("x-rapidapi-host", HOST).addHeader("x-rapidapi-key", KEY);
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();
        return okHttpClient;
    }




    }
