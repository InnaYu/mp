package api.tempMail;

import Retrofit.ApiClientBuilder;
import io.qameta.allure.Step;
import junit.framework.Assert;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static util.TempMail.generateMd5HashCodeFromEmail;

public class TempMailApi {
    public static TempMailController tempMailController = ApiClientBuilder.build(TempMailController.class);


    @Step
    public static Response<ResponseBody> get(String id) {
        id = generateMd5HashCodeFromEmail(id);
        Response<ResponseBody> response = tempMailController.get(id);
        Assert.assertTrue(response.isSuccessful());
        return response;
    }
}
