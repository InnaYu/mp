package api.tempMail;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TempMailController {
    @GET("mail/id/{id}/")
    Response<ResponseBody> get(@Path("id") String id);
}
