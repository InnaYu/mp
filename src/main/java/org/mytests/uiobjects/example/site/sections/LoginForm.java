package org.mytests.uiobjects.example.site.sections;

import com.epam.jdi.light.elements.common.UIElement;
import com.epam.jdi.light.elements.composite.Form;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.Css;
import org.mytests.uiobjects.example.entities.User;

public class LoginForm extends Form<User> {

    @Css("[formcontrolname = 'login']")
    public static UIElement email;

    @Css("[formcontrolname = 'password']")
    public static UIElement password;

    @Css(".auth-modal__submit")
    public static UIElement submit;

    @Css(".auth-modal__register-link")
    public static UIElement registration;

    @Css(".auth-modal__form")
    public static RegistrationForm registrationForm;

    @Override
    public void fill(User user) {
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());

    }
    @Override
    public void submit( User user) {
        fill(user);
        submit.click();
    }




}
