package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.XPath;
import org.mytests.uiobjects.example.site.sections.Contacts;

public class PersonalInformation extends WebPage {

    @XPath("//rz-cabinet-contacts//section[@class='personal-section']")
    public Contacts contacts;
}
