package org.mytests.uiobjects.example.site.sections;

import com.epam.jdi.light.elements.common.UIElement;
import com.epam.jdi.light.elements.composite.Form;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.Css;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.XPath;

public class ConfirmForm extends Form {
    @XPath("//input[@class='ng-touched ng-dirty ng-valid']")
    public UIElement code;

    @Css(".button--green")
    public UIElement submit;

    @Css(".form__hint_type_success")
    public UIElement confirmEmailMessage;


}
