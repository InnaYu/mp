package org.mytests.uiobjects.example.site.sections;

import com.epam.jdi.light.elements.common.UIElement;
import com.epam.jdi.light.elements.composite.Form;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.Css;
import org.mytests.uiobjects.example.entities.User;


public class RegistrationForm extends Form<User> {

    @Css("[formcontrolname='name']")
    public static UIElement name;

    @Css("[formcontrolname = 'username']")
    public static UIElement email;

    @Css("[formcontrolname = 'password']")
    public static UIElement password;

    @Css(".auth-modal__submit")
    public static UIElement submit;

    @Override
    public void fill(User user) {
            name.sendKeys(user.getName());
            email.sendKeys(user.getEmail());
            password.sendKeys(user.getPassword());

    }
    @Override
    public void submit( User user) {
        fill(user);
        submit.click();
    }



}
