package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.base.UIBaseElement;
import com.epam.jdi.light.elements.common.UIElement;
import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.Css;
import org.mytests.uiobjects.example.site.sections.LoginForm;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.debugger.model.SearchMatch;

public class HomePage extends WebPage {

    @Css(".header-topline__user-link")
    public static UIElement loginFormButton;

    @Css(".auth-modal__form")
    public static LoginForm loginForm;

    @Css("search-form__input")
    public static UIElement search;
}
