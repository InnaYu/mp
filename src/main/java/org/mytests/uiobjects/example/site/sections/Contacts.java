package org.mytests.uiobjects.example.site.sections;

import com.epam.jdi.light.elements.common.UIElement;
import com.epam.jdi.light.elements.composite.Section;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.Css;

public class Contacts extends Section {

    @Css(".personal-section__edit")
    public UIElement editButton;

    @Css(".button--navy")
    public UIElement confirmButton;

    @Css(".ng-pristine")
    public ConfirmForm confirmForm;

}
