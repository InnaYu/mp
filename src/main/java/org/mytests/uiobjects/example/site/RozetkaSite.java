package org.mytests.uiobjects.example.site;

import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import org.mytests.uiobjects.example.site.pages.HomePage;
import org.mytests.uiobjects.example.site.pages.PersonalInformation;

public class RozetkaSite {

    @Url("/") @Title("Home Page")
    public static HomePage homePage;

    @Url("/cabinet/personal-information/") @Title("Personal Information")
    public static PersonalInformation personalInformation;


}
