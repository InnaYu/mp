package org.mytests.uiobjects.example;

import org.apache.commons.lang3.RandomStringUtils;
import org.mytests.uiobjects.example.entities.User;

import static util.Random.genPassword;
import static util.Random.genRandomAlphabeticString;

public class BaseDataProvider {

    public static User generateNewUser(){
        User user = new User();
        user.setName(genRandomAlphabeticString(10, 'а','я'));
        user.setEmail(RandomStringUtils.randomAlphabetic(10) + "@promail1.net");
        user.setPassword(genPassword());
        return user;

    }


}
